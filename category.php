<?php include 'header.php'; ?>

<div class="boxed_layout" style="margin-top: 30px">

    <!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

    <div class="page_wrapper">

        <div class="container">

            <div class="row">

                <aside class="col-md-2 col-sm-4">

                    <!-- - - - - - - - - - - - - - Today's deals - - - - - - - - - - - - - - - - -->

                    <section class="section_offset categories">
                        <h3 class="widget_title">Ürünlerimiz</h3>

                        <ul>
                            <li>
                                <a href="javascript:;">Kahvaltılıklar</a>
                            </li>

                            <li>
                                <a href="javascript:;">Vejetaryen</a>
                            </li>

                            <li>
                                <a href="javascript:;">Organik</a>
                            </li>

                            <li>
                                <a href="javascript:;">Glutensiz</a>
                            </li>

                            <li>
                                <a href="javascript:;">Diyabetik</a>
                            </li>
                        </ul>
                    </section>
                    <!--/ .section_offset-->

                    <!-- - - - - - - - - - - - - - End of today's deals - - - - - - - - - - - - - - - - -->

                </aside>
                <!--/ [col]-->

                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <main class="col-md-10 col-sm-8">

                    <div class="products">

                        <?php for ($i = 0; $i < 20; $i++): ?>

                            <div class="product">
                                <div class="inner">
                                    <img src="images/samples/anasayfa-urun.jpg" alt="">

                                    <h2>Alpen Light</h2>

                                    <h3>Summer Fruits</h3>

                                    <div class="actions">
                                        <a class="action details" style="width: 50%" href="javascript:;" data-modal-url="modals/quick_view.html">İncele</a>
                                        <a class="action add-to-cart" style="width: 50%" href="javascript:;">Sepete At</a>
                                    </div>
                                </div>
                            </div>

                        <?php endfor; ?>

                    </div>

                </main>
                <!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

            </div>
            <!--/ .row-->

        </div>
        <!--/ .container-->

    </div>
    <!--/ .page_wrapper-->

    <!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->

</div>

<?php include 'footer.php'; ?>