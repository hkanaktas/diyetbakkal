<?php include 'header.php'; ?>

    <div class="slider">
        <div class="podium">
            <div class="container">
                <div>
                    <a href="javascript:;">
                        <img src="images/samples/slider.png" alt=""/>
                    </a>
                    <div>
                        <img src="images/samples/slider.png" alt=""/>
                    </div>
                    <div>
                        <img src="images/samples/slider.png" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

    <div class="page_wrapper">

        <div class="container">

            <div class="row">

                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <main class="col-md-9">

                    <section class="filter-section">
                        <ul>
                            <li>
                                <a href="javascript:;">
                                    Gıda Ürünleri
                                </a>
                            </li>

                            <li>
                                <a href="javascript:;">
                                    Kahvaltılıklar
                                </a>
                            </li>

                            <li>
                                <a href="javascript:;">
                                    Vejetaryen
                                </a>
                            </li>

                            <li>
                                <a href="javascript:;">
                                    Organik
                                </a>
                            </li>
                        </ul>
                    </section>

                    <div class="products">

                        <?php for ($i = 0; $i < 20; $i++): ?>

                            <div class="product">
                                <div class="inner">
                                    <img src="images/samples/anasayfa-urun.jpg" alt="">

                                    <h2>Alpen Light</h2>

                                    <h3>Summer Fruits</h3>

                                    <div class="actions">
                                        <a class="action browse" href="javascript:;" data-modal-url="modals/quick_view.html">Gözat</a>
                                        <a class="action details" href="javascript:;">İncele</a>
                                        <a class="action add-to-cart" href="javascript:;">Sepete At</a>
                                    </div>
                                </div>
                            </div>

                        <?php endfor; ?>

                    </div>

                </main>
                <!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

                <!-- - - - - - - - - - - - - - Banners - - - - - - - - - - - - - - - - -->

                <aside class="col-md-3 has_mega_menu">

                    <!-- - - - - - - - - - - - - - Today's deals - - - - - - - - - - - - - - - - -->

                    <section class="section_offset diet_shop">

                        <h3 class="widget_title">Diyet Vitrini</h3>

                        <!-- - - - - - - - - - - - - - Carousel of today's deals - - - - - - - - - - - - - - - - -->

                        <div class="owl_carousel widgets_carousel" data-direction="vertical">

                            <?php for ($i = 0; $i < 4; $i++): ?>

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                            <div class="product_item">

                                <!-- - - - - - - - - - - - - - Thumbnail - - - - - - - - - - - - - - - - -->

                                <div class="image_wrap">

                                    <img src="images/samples/anasayfa-urun.jpg" style="width: 100%" alt="">

                                </div>
                                <!--/. image_wrap-->

                                <button class="add-to-cart">
                                    Sepete At
                                </button>

                            </div>
                            <!--/ .product_item-->

                            <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                            <?php endfor; ?>

                        </div>
                        <!--/ .widgets_carousel-->

                        <!-- - - - - - - - - - - - - - End of carousel of today's deals - - - - - - - - - - - - - - - - -->

                    </section>
                    <!--/ .section_offset-->

                    <!-- - - - - - - - - - - - - - End of today's deals - - - - - - - - - - - - - - - - -->

                </aside>
                <!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of banners - - - - - - - - - - - - - - - - -->

            </div>
            <!--/ .row-->

        </div>
        <!--/ .container-->

    </div>
    <!--/ .page_wrapper-->

    <!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->

</div>

<?php include 'footer.php'; ?>