<!DOCTYPE html>
<html lang="tr">
<head>
    <!-- Basic page needs
		============================================ -->
    <title>ShopMe | Home</title>
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile specific metas
		============================================ -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="images/fav_icon.ico">

    <!-- Google web fonts
		============================================ -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>

    <!-- Libs CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="js/arcticmodal/jquery.arcticmodal.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Theme CSS
		============================================ -->
    <link rel="stylesheet" href="js/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="js/owlcarousel/owl.carousel.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- JS Libs
		============================================ -->
    <script src="js/modernizr.js"></script>

    <!-- Old IE stylesheet
		============================================ -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="css/oldie.css">
    <![endif]-->
</head>
<body class="front_page">

<!-- - - - - - - - - - - - - - Old ie alert message - - - - - - - - - - - - - - - - -->

<!--[if lt IE 9]>

<div class="ie_alert_message">

    <div class="container">

        <div class="on_the_sides">

            <div class="left_side">

                <i class="icon-attention-5"></i> <span class="bold">Attention!</span> This page may not display
                correctly. You are using an outdated version of Internet Explorer. For a faster, safer browsing
                experience.</span>

            </div>

            <div class="right_side">

                <a target="_blank"
                   href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                   class="button_black">Update Now!</a>

            </div>

        </div>

    </div>

</div>

<![endif]-->

<!-- - - - - - - - - - - - - - End of old ie alert message - - - - - - - - - - - - - - - - -->

<!-- - - - - - - - - - - - - - Main Wrapper - - - - - - - - - - - - - - - - -->

<div class="wide_layout">

    <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

    <header id="header">

        <!-- - - - - - - - - - - - - - Top part - - - - - - - - - - - - - - - - -->

        <div class="top_part">
            <div class="container" style="position: relative">
                <div style="text-align: right">

                    <!-- - - - - - - - - - - - - - Login - - - - - - - - - - - - - - - - -->

                    <p>
                        <a href="#" data-modal-url="modals/login.html">Giriş yap</a> veya
                        <a href="#">kayıt ol</a>
                    </p>

                    <!-- - - - - - - - - - - - - - End login - - - - - - - - - - - - - - - - -->

                </div>
                <!--/ [col]-->

            </div>
            <!--/ .container -->

        </div>
        <!--/ .top_part -->

        <!-- - - - - - - - - - - - - - End of top part - - - - - - - - - - - - - - - - -->

        <!-- - - - - - - - - - - - - - Bottom part - - - - - - - - - - - - - - - - -->

        <div class="bottom_part">

            <div class="container">

                <div class="row">

                    <div class="main_header_row">

                        <div class="col-md-3" style="text-align: center">

                            <!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->

                            <a href="index.php" class="logo">

                                <img src="images/logo.png" alt="">

                            </a>

                            <!-- - - - - - - - - - - - - - End of logo - - - - - - - - - - - - - - - - -->

                        </div>
                        <!--/ [col]-->

                        <div class="col-lg-7 col-md-6 col-sm-6">

                            <div class="call_us">

                                <span>Telefon:</span> <b>+90 (234) 5678</b>

                            </div><!--/ .call_us-->

                            <!-- - - - - - - - - - - - - - End call to action - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Search form - - - - - - - - - - - - - - - - -->

                            <form class="clearfix search">

                                <input type="text" name="" tabindex="1" placeholder="Search..." class="alignleft">

                                <!-- - - - - - - - - - - - - - Categories - - - - - - - - - - - - - - - - -->

                                <div class="search_category alignleft">

                                    <div class="open_categories">Tüm Kategoriler</div>

                                    <ul class="categories_list dropdown">

                                        <li class="animated_item" style="transition-delay:0.1s"><a href="#">Kategori</a></li>
                                        <li class="animated_item" style="transition-delay:0.2s"><a href="#">Kategori</a></li>
                                        <li class="animated_item" style="transition-delay:0.3s"><a href="#">Kategori</a></li>
                                        <li class="animated_item" style="transition-delay:0.4s"><a href="#">Kategori</a></li>
                                        <li class="animated_item" style="transition-delay:0.5s"><a href="#">Kategori</a></li>
                                        <li class="animated_item" style="transition-delay:0.6s"><a href="#">Kategori</a></li>
                                        <li class="animated_item" style="transition-delay:0.7s"><a href="#">Kategori</a></li>

                                    </ul>

                                </div><!--/ .search_category.alignleft-->

                                <!-- - - - - - - - - - - - - - End of categories - - - - - - - - - - - - - - - - -->

                                <button style="background: #8ec53e" class="button_blue def_icon_btn alignleft"></button>

                            </form><!--/ #search-->

                            <!-- - - - - - - - - - - - - - End search form - - - - - - - - - - - - - - - - -->

                        </div>

                        <div class="col-lg-3 col-sm-3">

                            <div class="align_right">

                                <!-- - - - - - - - - - - - - - Shopping cart - - - - - - - - - - - - - - - - -->

                                <div class="shopping_cart_wrap">

                                    <button id="open_shopping_cart" class="open_button" data-amount="3">
                                        <b class="title">Sepet</b>
                                        <b class="total_price">999.00 TL</b>
                                    </button>

                                    <!-- - - - - - - - - - - - - - Products list - - - - - - - - - - - - - - - - -->

                                    <div class="shopping_cart dropdown">

                                        <?php for ($i = 0; $i < 4; $i++): ?>

                                        <div class="animated_item">

                                            <p class="title">Son Eklenenler</p>

                                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                            <div class="clearfix sc_product">

                                                <a href="#" class="product_thumb"><img src="images/sc_img_1.jpg" alt=""></a>

                                                <a href="#" class="product_name">Alpen Light</a>

                                                <p>1 x 499.00 TL</p>

                                                <button class="close"></button>

                                            </div><!--/ .clearfix.sc_product-->

                                            <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .animated_item-->

                                        <?php endfor; ?>

                                    </div><!--/ .shopping_cart.dropdown-->

                                    <!-- - - - - - - - - - - - - - End of products list - - - - - - - - - - - - - - - - -->

                                </div><!--/ .shopping_cart_wrap.align_left-->

                                <!-- - - - - - - - - - - - - - End of shopping cart - - - - - - - - - - - - - - - - -->

                            </div><!--/ .align_right-->

                        </div>
                    </div>
                    <!--/ .main_header_row-->

                </div>
                <!--/ .row-->

            </div>
            <!--/ .container-->

        </div>
        <!--/ .bottom_part -->

        <!-- - - - - - - - - - - - - - End of bottom part - - - - - - - - - - - - - - - - -->

        <!-- - - - - - - - - - - - - - Main navigation wrapper - - - - - - - - - - - - - - - - -->

        <div id="main_navigation_wrap">

            <div class="container">

                <div class="row">

                    <div class="col-xs-12">

                        <!-- - - - - - - - - - - - - - Sticky container - - - - - - - - - - - - - - - - -->

                        <div class="sticky_inner type_2">

                            <!-- - - - - - - - - - - - - - Navigation item - - - - - - - - - - - - - - - - -->

                            <div class="nav_item">

                                <nav class="main_navigation">

                                    <ul>
                                        <li class="current">
                                            <a href="index.php"><i class="icon-cutlery"></i> Anasayfa</a>
                                        </li>

                                        <li>
                                            <a href="category.php"><i class="icon-cutlery"></i> Kategori Sayfası</a>
                                        </li>

                                        <li>
                                            <a href="product.php"><i class="icon-cutlery"></i> Ürün Sayfası</a>
                                        </li>
                                    </ul>

                                </nav>
                                <!--/ .main_navigation-->

                            </div>

                            <!-- - - - - - - - - - - - - - End of navigation item - - - - - - - - - - - - - - - - -->

                        </div>
                        <!--/ .sticky_inner -->

                        <!-- - - - - - - - - - - - - - End of sticky container - - - - - - - - - - - - - - - - -->

                    </div>
                    <!--/ [col]-->

                </div>
                <!--/ .row-->

            </div>
            <!--/ .container-->

        </div>
        <!--/ .main_navigation_wrap-->

        <!-- - - - - - - - - - - - - - End of main navigation wrapper - - - - - - - - - - - - - - - - -->

    </header>

    <!-- - - - - - - - - - - - - - End Header - - - - - - - - - - - - - - - - -->
