<?php include 'header.php'; ?>

    <div class="boxed_layout" style="margin-top: 30px">

        <!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

        <div class="page_wrapper">

            <div class="container">

                <div class="row">

                    <aside class="col-md-2 col-sm-4">

                        <!-- - - - - - - - - - - - - - Today's deals - - - - - - - - - - - - - - - - -->

                        <section class="section_offset categories">
                            <h3 class="widget_title">Ürünlerimiz</h3>

                            <ul>
                                <li class="active">
                                    <a href="javascript:;">Kahvaltılıklar</a>
                                </li>

                                <li>
                                    <a href="javascript:;">Vejetaryen</a>
                                </li>

                                <li>
                                    <a href="javascript:;">Organik</a>
                                </li>

                                <li>
                                    <a href="javascript:;">Glutensiz</a>
                                </li>

                                <li>
                                    <a href="javascript:;">Diyabetik</a>
                                </li>
                            </ul>
                        </section>
                        <!--/ .section_offset-->

                        <!-- - - - - - - - - - - - - - End of today's deals - - - - - - - - - - - - - - - - -->

                    </aside>
                    <!--/ [col]-->

                    <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                    <main class="col-md-10 col-sm-8 product">

                        <div class="row main-content">
                            <div class="col-sm-5">
                                <img src="images/samples/urun-buyuk.jpg" style="width: 100%" alt="">
                            </div>

                            <div class="col-sm-7 details">
                                <h1>Alpro Soya</h1>
                                <h5>1 lt.</h5>

                                <div class="price" style="display: table-row">
                                    <div style="width: 100px; display: table-cell">
                                        <input type="number" value="1" min="1" max="10">
                                    </div>

                                    <div style="display: table-cell; padding-left: 25px; vertical-align: middle">12,50 TL</div>
                                </div>

                                <div class="actions">
                                    <div style="width: 50%; padding: 0 10px">
                                        <a class="action details" href="javascript:;">Satın Al</a>
                                    </div>

                                    <div style="width: 50%; padding: 0 10px">
                                        <a class="action add-to-cart" href="javascript:;">Sepete At</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="products four-in-row">

                            <?php for ($i = 0; $i < 4; $i++): ?>

                                <div class="product">
                                    <div class="inner">
                                        <img src="images/samples/anasayfa-urun.jpg" alt="">

                                        <h2>Alpen Light</h2>

                                        <h3>Summer Fruits</h3>

                                        <div class="actions">
                                            <a class="action details" style="width: 50%" href="javascript:;" data-modal-url="modals/quick_view.html">İncele</a>
                                            <a class="action add-to-cart" style="width: 50%" href="javascript:;">Sepete At</a>
                                        </div>
                                    </div>
                                </div>

                            <?php endfor; ?>

                        </div>


                    </main>
                    <!--/ [col]-->

                    <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

                </div>
                <!--/ .row-->

            </div>
            <!--/ .container-->

        </div>
        <!--/ .page_wrapper-->

        <!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->

    </div>

<?php include 'footer.php'; ?>