
<!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

<div id="footer" class="boxed_layout">
    <div class="corners">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>

    <footer>
        <div class="footer_section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 logo">
                        <img src="images/footer-logo.png" alt="">
                    </div>

                    <div class="col-md-8 col-sm-6">
                        <section class="widget sitemap">
                            <h4>Site Haritası</h4>

                            <ul class="list_of_links">
                                <li><a href="#">Gıda Ürünleri</a></li>
                                <li><a href="#">Kahvaltılıklar</a></li>
                                <li><a href="#">Vejetaryen</a></li>
                                <li><a href="#">Organik</a></li>
                                <li><a href="#">Tarifler</a></li>
                                <li><a href="#">Glutensiz</a></li>
                                <li><a href="#">Diyabetik</a></li>
                                <li><a href="#">Atıştırmalık ve Tatlılar</a></li>
                                <li><a href="#">İçecekler</a></li>
                                <li><a href="#">Mutfak Araçları</a></li>
                            </ul>
                        </section>

                        <div class="row" style="margin-top: 30px">
                            <div class="col-sm-4 socials">
                                <h4>Sosyal Medya</h4>

                                <ul>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="icon icon-facebook-1"></i>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="javascript:;">
                                            <i class="icon icon-twitter"></i>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="javascript:;">
                                            <i class="icon icon-pinterest"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-sm-4 col-sm-offset-4">
                                <h4>İletişim</h4>

                                <p>1423 sk. Alsancak / İzmir<br>+90 532 456 78 89</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<!-- - - - - - - - - - - - - - End Footer - - - - - - - - - - - - - - - - -->

<!-- Include Libs & Plugins
============================================ -->
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/owlcarousel/owl.carousel.min.js"></script>
<script src="twitter/jquery.tweet.min.js"></script>
<script src="js/arcticmodal/jquery.arcticmodal.js"></script>
<script src="js/colorpicker/colorpicker.js"></script>
<script src="js/retina.min.js"></script>
<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js"></script>

<!-- Theme files
============================================ -->
<script src="js/theme.plugins.js"></script>
<script src="js/theme.core.js"></script>

</body>
</html>